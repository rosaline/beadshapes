package com.rosalinesrandoms.beadshapes.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rosalinesrandoms.beadshapes.R;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

	static {		
		addItem(new DummyItem("1", "Barrel", R.drawable.ic_barrel, R.drawable.barrel));
		addItem(new DummyItem("2", "Bugles", R.drawable.ic_bugle, R.drawable.bugle));
		addItem(new DummyItem("3", "Corolla", R.drawable.ic_corolla, R.drawable.corolla));
		addItem(new DummyItem("4", "Drop", R.drawable.ic_drop, R.drawable.drop));
		addItem(new DummyItem("5", "Drum", R.drawable.ic_drum, R.drawable.drum));
		addItem(new DummyItem("6", "Ersatz", R.drawable.ic_ersatz, R.drawable.ersatz));
		addItem(new DummyItem("7", "Farfalle/Berry/Peanut", R.drawable.ic_farfalle, R.drawable.farfalle));
		addItem(new DummyItem("8", "Square", R.drawable.ic_square, R.drawable.square));
		addItem(new DummyItem("9", "Three-cut", R.drawable.ic_threecut, R.drawable.threecut));
		addItem(new DummyItem("10", "Triangle", R.drawable.ic_triangle, R.drawable.triangle));
		addItem(new DummyItem("11", "Tube", R.drawable.ic_tube, R.drawable.tube));
		addItem(new DummyItem("12", "Twin/Tila", R.drawable.ic_twin, R.drawable.twin));
		addItem(new DummyItem("13", "Two-cut/Hexagon", R.drawable.ic_twocut, R.drawable.twocut));
		addItem(new DummyItem("14", "Oblong", R.drawable.ic_oblong, R.drawable.oblong));
		addItem(new DummyItem("15", "Oval", R.drawable.ic_oval, R.drawable.oval));
		addItem(new DummyItem("16", "Pipe", R.drawable.ic_pipe, R.drawable.pipe));
		addItem(new DummyItem("17", "Round/Rocailles", R.drawable.ic_rocailles, R.drawable.rocailles));
		addItem(new DummyItem("18", "Round", R.drawable.ic_round, R.drawable.round));
		addItem(new DummyItem("19", "Round Tube", R.drawable.ic_round_tube, R.drawable.round_tube));
		
		
		//as a hack add an extra copy of the last one to appear under the banner
		addItem(new DummyItem("20", "Round Tube", R.drawable.ic_round_tube, R.drawable.round_tube));
	}

	private static void addItem(DummyItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	/**
	 * A dummy item representing a piece of content.
	 */
	public static class DummyItem {
		public String id;
		public String content;
		public int listPicture;
		public int detailPicture;

		public DummyItem(String id, String content, int pictureID, int detailID) {
			this.id = id;
			this.content = content;
			this.listPicture = pictureID;
			this.detailPicture = detailID;
		}

		@Override
		public String toString() {
			return content;
		}
	}
}
