package com.rosalinesrandoms.beadshapes;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.LinearLayout;

/**
 * An activity representing a list of Bead Shapes. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link BeadShapeDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link BeadShapeListFragment} and the item details (if present) is a
 * {@link BeadShapeDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link BeadShapeListFragment.Callbacks} interface to listen for item
 * selections.
 */
public class BeadShapeListActivity extends FragmentActivity implements
		BeadShapeListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;

	  /** The view to show the ad. */
	  private AdView adView;

	  /* Your ad unit id. Replace with your actual ad unit id. */
	  private static final String AD_UNIT_ID = "INSERT_YOUR_AD_UNIT_ID_HERE";
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_beadshape_list);

		if (findViewById(R.id.beadshape_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((BeadShapeListFragment) getSupportFragmentManager()
					.findFragmentById(R.id.beadshape_list))
					.setActivateOnItemClick(true);
		}
		

			  // Create an ad.
		    //adView = new AdView(this);
			
			adView = (AdView) findViewById(R.id.adView);
			 AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("A6826F69A10F641175D4B90EAB87D1FE").build();
			
			    adView.loadAd(adRequest);
		   // adView.setAdSize(AdSize.BANNER);
		   // adView.setAdUnitId(AD_UNIT_ID);

		    // Add the AdView to the view hierarchy. The view will have no size
		    // until the ad is loaded.
		   // LinearLayout layout = (LinearLayout) findViewById(R.id.list_view_activity);
		    //layout.addView(adView);

		    // Create an ad request. Check logcat output for the hashed device ID to
		    // get test ads on a physical device.
		  //  AdRequest adRequest = new AdRequest.Builder()
		   //     .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		    //    .addTestDevice("INSERT_YOUR_HASHED_DEVICE_ID_HERE")
		    //    .build();

		    // Start loading the ad in the background.
		    //adView.loadAd(adRequest);
		
		
		// TODO: If exposing deep links into your app, handle intents here.
		
	}

	/**
	 * Callback method from {@link BeadShapeListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(BeadShapeDetailFragment.ARG_ITEM_ID, id);
			BeadShapeDetailFragment fragment = new BeadShapeDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.beadshape_detail_container, fragment)
					.commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this,
					BeadShapeDetailActivity.class);
			detailIntent.putExtra(BeadShapeDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}
}
