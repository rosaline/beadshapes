package com.rosalinesrandoms.beadshapes;

import java.util.List;

import com.rosalinesrandoms.beadshapes.dummy.DummyContent;
import com.rosalinesrandoms.beadshapes.dummy.DummyContent.DummyItem;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

public class shapeArrayAdapter<DummyItem> extends ArrayAdapter<Object> {
	  private final Context context;
	  private final List<DummyContent.DummyItem> values;


	public shapeArrayAdapter(Context context, int resource,
			int textViewResourceId, List values) {
		super(context, resource, textViewResourceId, values);
		// TODO Auto-generated constructor stub
		
	    this.context = context;
	    this.values = values;
	}


	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(values.get(position).content);
	    // Change the icon for Windows and iPhone
	    imageView.setImageResource(values.get(position).listPicture);
	    

	    return rowView;
	  }

}
